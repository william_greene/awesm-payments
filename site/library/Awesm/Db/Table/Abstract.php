<?php

abstract class Awesm_Db_Table_Abstract extends Zend_Db_Table
{
    
    private $adapter;
    private $db;
 
    function Awesm_Db_Table_Abstract($adapter = null)
    {
        if(isset($adapter))
        {
            $dbAdapters = Zend_Registry::get('dbAdapters');
            $config = ($dbAdapters[$adapter]);
        }
        parent::__construct($config);
    }
 
}