<?php 

/**
 * IndexController is an exception; it gets loaded for the / route by default.
 * Other controllers are by controller/action, e.g. /users/edit is UserController/editAction
 */ 
class IndexController extends Zend_Controller_Action 
{ 
    public function indexAction() 
    {
        // do controller stuff here

        // views/index/index.phtml will get rendered
        
    	$users = new Model_Users();
    	
    	$this->view->validSession = $users->hasValidSession();
    	
    } 
}
