<?php

class Model_Users extends Awesm_Db_Table_Abstract {

	private $adapter = 'redirectionsdb';
	private $db;

	function __construct()
	{
		parent::__construct($this->adapter);
		$this->db = $this->getAdapter();
	}

	/**
	 * Returns true if this sharer_id is owned by a user authorized for this account_id
	 * @param unknown_type $sharer_id
	 * @param unknown_type $account_id
	 */
	function validateSharerIdForAccount($sharer_id, $account_id)
	{
		$st = $this->db->prepare('SELECT a.id FROM users a, memberships b WHERE a.snowball_id = ? AND a.id = b.user_id AND b.account_id = ? LIMIT 1');
		$st->execute(array($sharer_id, $account_id));
		return $st->fetchColumn();
	}

	/**
	 * Get all account IDs for a user
	 * @param unknown_type $user_id
	 */
	function getAccounts($user_id){
		$st = $this->db->prepare('SELECT account_id FROM memberships WHERE user_id = ?');
		$st->execute(array($user_id));
		$accounts = array();
		while($account_id = $st->fetchColumn())
		{
			$accounts[] = $account_id;
		}
		return $accounts;
	}
	
	/**
	 * Gets an ID and token from a user's cookies. Compares them to values in
	 * memcache to check that user has a valid session from the rails app.
	 * 
	 * Mechanism is simple:
	 * 1. In rails, we give the user a sharer_id, and generate a token
	 * 2. Still in rails, we encrypt the token and give them the encrypted token (sToken)
	 * 3. We store the plaintext token in memcache.
	 * 4. In PHP, we read the sharer_id and sToken from their cookies
	 * 5. We look up the token for their sharer_id in memcache
	 * 6. We encrypt the original token in the same way (vToken)
	 * 7. If the sToken and vToken match, they are the same person who logged into rails
	 * 8. If the timestamp stored in memcache is reasonably fresh, we let them in
	 * 
	 * @param $snowballId
	 * @param $sToken
	 */
	function hasValidSession($snowballId=null,$sToken=null)
	{
		$config = Zend_Registry::get('configuration');
		$seed = $config->sharedsessions->seed;
		$lifetime = $config->sharedsessions->lifetime;
		
		if(empty($snowballId)) $snowballId = $_COOKIE['snowball'];
		if(empty($sToken)) $sToken = $_COOKIE['stoken'];
	
		// the key is a seeded hash so our memcache keys can't be guessed if we accidentally expose it
		$snowballIdKey = 'session_' . sha1($seed.$snowballId);
		
		// error_log("Looking for state data in snowball id key $snowballIdKey");
		$cache = Zend_Registry::get('cache');
		$response = $cache->get($snowballIdKey);
		
		// no session data at all: automatic fail
		if (empty($response)) return false;
		
		// decode the memcache value to get the token and timestamp
		$result = json_decode($response,true);
		if (empty($result)) {
			throw new Exception("Session data was invalid.");
		}

		// our verification token & timestamp
		$vToken = sha1($seed.$result['token']);
		$timestamp = strtotime($result['timestamp']);

		// token is good?
		if ($vToken == $sToken)
		{
			// timestamp fresh enough?
			if ($timestamp > (time() - $lifetime))
			{
				// yay!
				return true;
			}
			else
			{
				error_log("Session for sharer ID $snowballId has expired");
			}
		}
		else
		{
			error_log("Tokens don't match. Old session on a different machine, or possible spoof attempt");
		}
		return false;
	}
	
}
