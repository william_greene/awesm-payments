<?php
// APPLICATION CONSTANTS - Set the constants to use in this application.
// These constants are accessible throughout the application, even in ini 
// files. We optionally set APPLICATION_PATH here in case our entry point 
// isn't index.php (e.g., if required from our test suite or a script).
defined('APPLICATION_PATH')
    or define('APPLICATION_PATH', dirname(__FILE__));

$env = getenv('ZEND_ENV');
if (empty($env)) $env = 'production';
defined('APPLICATION_ENVIRONMENT')
    or define('APPLICATION_ENVIRONMENT', $env);
    
// FRONT CONTROLLER - Get the front controller.
// The Zend_Front_Controller class implements the Singleton pattern, which is a
// design pattern used to ensure there is only one instance of
// Zend_Front_Controller created on each request.
$frontController = Zend_Controller_Front::getInstance();

// CONTROLLER DIRECTORY SETUP - Point the front controller to your action
// controller directory.
$frontController->setControllerDirectory(APPLICATION_PATH . '/controllers');

// APPLICATION ENVIRONMENT - Set the current environment
// Set a variable in the front controller indicating the current environment --
// commonly one of development, staging, testing, production, but wholly
// dependent on your organization and site's needs.
$frontController->setParam('env', APPLICATION_ENVIRONMENT);

// LAYOUT SETUP - Setup the layout component
// The Zend_Layout component implements a composite (or two-step-view) pattern
// In this call we are telling the component where to find the layouts scripts.
Zend_Layout::startMvc(APPLICATION_PATH . '/layouts/scripts');

// VIEW SETUP - Initialize properties of the view object
// The Zend_View component is used for rendering views. Here, we grab a "global"
// view instance from the layout object, and specify the doctype we wish to
// use -- in this case, XHTML1 Strict.
$view = Zend_Layout::getMvcInstance()->getView();
$view->doctype('HTML4_STRICT');

// CONFIGURATION - Setup the configuration object
// The Zend_Config_Ini component will parse the ini file, and resolve all of
// the values for the given section.  Here we will be using the section name
// that corresponds to the APP's Environment
if (!$configuration = apc_fetch('configuration'))
{
	error_log("--- Loading config from app.ini ----");
	$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/config/app.ini', APPLICATION_ENVIRONMENT);
	apc_store('configuration',$configuration);
}

// DATABASE ADAPTER - Setup the database adapter
// Zend_Db implements a factory interface that allows developers to pass in an
// adapter name and some parameters that will create an appropriate database
// adapter object.  In this instance, we will be using the values found in the
// "database" section of the configuration obj.
$dbAdapters = array(
    'redirectionsdb' => Zend_Db::factory($configuration->redirections) 
);

// DATABASE TABLE SETUP - Setup the Database Table Adapter
// Since our application will be utilizing the Zend_Db_Table component, we need 
// to give it a default adapter that all table objects will be able to utilize 
// when sending queries to the db.
Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapters['redirectionsdb']); // most queries

/*
 * These classes automatically serialize stuff, which means it's impossible to
 * get values from ruby. So we use the library directly.
// CACHE
// configure caching backend strategy
$cacheBackend = new Zend_Cache_Backend_Memcached(
    array(
        'servers' => array( array(
            'host' => $configuration->memcache->server,
            'port' => '11211'
        ) ),
        'compression' => false
    ) 
);
// configure caching frontend strategy
$cacheFrontend = new Zend_Cache_Core(
    array(
        'caching' => $configuration->memcache->caching,
        'cache_id_prefix' => '',
        'lifetime' => $configuration->memcache->lifetime,
        'write_control' => true,
        'automatic_serialization' => true,
        'ignore_user_abort' => true
    ) 
);
// build a caching object
$cache = Zend_Cache::factory($cacheFrontend,$cacheBackend);
*/

$cache = new Memcache;
$cache->connect($configuration->memcache->server, 11211) or die ("Could not connect to memcache");

// REGISTRY - setup the application registry
// An application registry allows the application to store application 
// necessary objects into a safe and consistent (non global) place for future 
// retrieval.  This allows the application to ensure that regardless of what 
// happends in the global scope, the registry will contain the objects it 
// needs.
$registry = Zend_Registry::getInstance();
$registry->configuration = $configuration;
$registry->dbAdapters    = $dbAdapters;
$registry->cache         = $cache;

// AUTH - authentication
// $auth = Zend_Auth::getInstance();

// ROUTING
$ctrl = Zend_Controller_Front::getInstance();
$router = $ctrl->getRouter();

///////////////////////////// v2 routes //////////////////////

$router->addRoute( 'shareslookup',
    new Zend_Controller_Router_Route_Regex(
    	'shares(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'redirections',
            'action'     => 'lookup'
        ),
        array(
            2 => 'format'
        )
    )
);

$router->addRoute( 'clickslookup',
    new Zend_Controller_Router_Route_Regex(
    	'clicks(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'consumptions',
            'action'     => 'lookup'
        ),
        array(
            2 => 'format'
        )
    )
);

$router->addRoute( 'clickdetails',
    new Zend_Controller_Router_Route_Regex(
    	'clicks/(.+)_([A-Za-z0-9]+)(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'consumptions',
            'action'     => 'stub'
        ),
        array(
            1 => 'domain',
            2 => 'stub',
            4 => 'format'  //check to see if this when blank overwrite a format var specified on the GET string of the url
        )
    )
);

$router->addRoute( 'shareslist',
    new Zend_Controller_Router_Route_Regex(
    	'shares/list(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'redirections',
            'action'     => 'list'
        ),
        array(
            2 => 'format'
        )
    )
);

$router->addRoute( 'clickslist',
    new Zend_Controller_Router_Route_Regex(
    	'clicks/list(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'consumptions',
            'action'     => 'list'
        ),
        array(
            2 => 'format'
        )
    )
);

$router->addRoute( 'activity',
    new Zend_Controller_Router_Route_Regex(
    	'activity(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'activity',
            'action'     => 'lookup'
        ),
        array(
            2 => 'format'
        )
    )
);

//////////////////// v1 routes //////////////////////

$router->addRoute( 'v1lookup',
    new Zend_Controller_Router_Route_Regex(
    	'url(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'legacy',
            'action'     => 'lookup'
        ),
        array(
            2 => 'format'
        )
    )
);            

$router->addRoute( 'v1info',
    new Zend_Controller_Router_Route_Regex(
    	'url/([A-Za-z0-9]+)(\.(xml|json|XML|JSON))?',
        array(
            'controller' => 'legacy',
            'action'     => 'info'
        ),
        array(
        	1 => 'stub',
            3 => 'format'
        )
    )
);        

////////////////////// example route ///////////////////
$router->addRoute( 'arbitraryname', 
    new Zend_Controller_Router_Route(
        'any/path/:var1/:var2',
        array(
            'controller' => 'anycontroller',
            'action' => 'anyaction',
            'extraparm' => 'arbitraryvalue'
        )
    )
);

// PLUGIN REGISTRATION
// if any

// CLEANUP - remove items from global scope
// This will clear all our local boostrap variables from the global scope of 
// this script (and any scripts that called bootstrap).  This will enforce 
// object retrieval through the Applications's Registry
unset($frontController, $view, $configuration, $dbAdapters, $registry, $auth, $router, $cache);
